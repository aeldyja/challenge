#!/bin/bash

i=0
last_char=""
while read -n1 c; do
   char_hex=$(printf '%x' "'$c")
   if [ ! -z "${last_char}" ]; then
       echo "$char_hex" > "$(printf '%03d' "$i")-${last_char}"
   fi
   last_char=$char_hex
   ((i++))
done < "$1"