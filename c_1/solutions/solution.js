#!/usr/bin/env node

let BASE_URL = "http://challenge_baseurl/c_1/subdir/";

let result_array = ["4c"];

function zeroFill( number, width )
{
    width -= number.toString().length;
    if ( width > 0 )
    {
        return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
    }
    return number + ""; // always return a string
}

function hex2a(hexx) {
    var hex = hexx.toString();//force conversion
    var str = '';
    for (var i = 0; (i < hex.length && hex.substr(i, 2) !== '00'); i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}

var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    //console.log(this.readyState, '-', this.status);
    if (this.readyState == 4 && this.status == 200) {
        // Typical action to be performed when the document is ready:
        console.log(xhttp.responseText)
        result_array.push(xhttp.responseText);
    }
};

for (var i = 1, len = 193; i <= len; i++) {
    console.log(BASE_URL + zeroFill(i, 3) + "-" + result_array[result_array.length - 1]);
    xhttp.open("GET", BASE_URL + zeroFill(i, 3) + "-" + result_array[result_array.length - 1], false);
    xhttp.send();
}

console.log(result_array)

let result_str = ""

for (const element of result_array) {
    result_str += hex2a(element.replace(/\n/g, ''))
}

console.log(result_str)
