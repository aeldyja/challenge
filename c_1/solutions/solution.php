#!/usr/bin/env php

<?php

$BASE_URL="http://challenge_baseurl/c_1/subdir/";

$result_array = array('4c');

function hex2str($hex) {
    $str = '';
    for($i=0;$i<strlen($hex);$i+=2) $str .= chr(hexdec(substr($hex,$i,2)));
    return $str;
}

for ($i = 1; $i <= 193; $i++) {
    //loop over i and send http request to url
    echo $BASE_URL . sprintf("%03d", $i) . "-" . end($result_array) ."\n";
    array_push($result_array, preg_replace("/\n/", "", file_get_contents($BASE_URL . sprintf("%03d", $i) . "-" . end($result_array) )));
}

var_dump($result_array);

$result_str = "";
foreach ($result_array as $char) {
    $result_str .= hex2str($char);
}

var_dump($result_str)
?>
