#!/bin/bash

BASE_URL="http://challenge_baseurl/c_1/subdir/"

result_array=("4c")

for (( i = 1; i <= 193; i++ )); do #Loop over URLS to constitue an array of the obtained alphanumeric chars
    echo "${BASE_URL}$(printf '%03d' "$i")-${result_array[-1]}"
    result_array+=($(curl --silent "${BASE_URL}$(printf '%03d' "$i")-${result_array[-1]}")) #Use current indentation zerofilled and last obtained AN code
done

#declare -p result_array

# Decode array from hexadecimal

result_string=""

for entry in "${result_array[@]}"; do
    result_string+="$(echo "${entry}" | xxd -r -p)"
done

echo "${result_string}"
